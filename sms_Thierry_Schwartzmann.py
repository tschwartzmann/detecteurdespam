#Thierry Schwartzmann (21208197)
#!/usr/lib/python3.4
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score
import warnings as wa

#####################################################################################
############################### Ouverture du fichier ################################
#####################################################################################

# ouverture du fichier SMSSpamCollection en lecture
fichier = open('SMS_Spam/SMSSpamCollection.txt','r')

# parcours des lignes du fichier
sms_lines = fichier.readlines()

# nombre de lignes du fichier
sms_len = len(sms_lines)

#####################################################################################

#####################################################################################
############################### Séparation de la base ###############################
#####################################################################################

# base d'entrainement = 30% de la base de sms
tp = round(sms_len*0.3) # 30% des sms
train_label=[] # label : ham ou spam
train_text=[] # text : contenu des messages
for i in range(tp):
	lignes = sms_lines[i].split("\t") # ajout de la tabulation entre label et text
	train_label.append(lignes[0]) # ajout des éléments dans la colonne des label
	train_text.append(lignes[1]) # ajout des éléments dans la colonne de text

# base de tests = reste de la base
test_label=[] # label : ham ou spam
test_text=[] # text : contenu des messages
for i in range(tp,sms_len):
	lignes = sms_lines[i].split("\t") # ajout de la tabulation entre label et text
	test_label.append(lignes[0]) # ajout des éléments dans la colonne des label
	test_text.append(lignes[1]) # ajout des éléments dans la colonne de text

#####################################################################################

#####################################################################################
############################### Définition des descripteurs #########################
#####################################################################################

# Descripteur qui permet de transformer le texte en matrice
class DenseTransformer(TfidfTransformer):

    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self

# fonction qui transforme les valeurs ham et spam en binaire (spam = 1 et ham = 0)
def transformationBinaire(label):
	labelEncoder = LabelEncoder()
	valeurBinaire = labelEncoder.fit_transform(label)
	return valeurBinaire

#####################################################################################

#####################################################################################
################################ Choix des classifieurs #############################
#####################################################################################

wa.filterwarnings('ignore')

## Arbre de décision
# Arbre de decision (entropie de Shannon)
clf1 = Pipeline([('vect', CountVectorizer()),
				('densetransform', DenseTransformer()),
				('clf', DecisionTreeClassifier(criterion='entropy')),
	])

tree_entropy = clf1.fit(train_text, train_label)
prediction = tree_entropy.predict(test_text)
print('-'*60)
print('Arbre de décision : entropie de Shannon')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %tree_entropy.score(test_label, prediction))
# aire sous la courbe ROC
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Arbre de decision (index de gini)
clf2 = Pipeline([('vect', CountVectorizer()),
				('densetransform', DenseTransformer()),
				('clf', DecisionTreeClassifier(criterion='gini')),
	])

tree_gini = clf2.fit(train_text, train_label)
prediction = tree_gini.predict(test_text)
print('Arbre de décision : index de gini')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %tree_gini.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)


## Support Vector Machine
# Support Vector Machine SVC(kernel linear)
# ignorer les avertissements
clf3 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', SVC(kernel='linear', C=1.0)),
	])

svc_linear = clf3.fit(train_text, train_label)
prediction = svc_linear.predict(test_text)
print('SVC noyau linéaire')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %svc_linear.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Support Vector Machine SVC(kernel rbf ou gaussien)
clf4 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', SVC(kernel='rbf', C=1.0, gamma=0.8)),
	])

svc_rbf = clf4.fit(train_text, train_label)
prediction = svc_rbf.predict(test_text)
print('SVC noyau gaussien')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %svc_rbf.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Support Vector Machine SVC(kernel polynomial)
clf5 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', SVC(kernel='poly', C=1.0, degree=7, gamma=0.8)),
	])

svc_poly = clf5.fit(train_text, train_label)
prediction = svc_poly.predict(test_text)
print('SVC noyau polynomial')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %svc_poly.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Support Vector Machine LinearSVC
clf6 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', LinearSVC(C=1.0, dual=False, tol=0.001)),
	])

lin_svc = clf6.fit(train_text, train_label)
prediction = lin_svc.predict(test_text)
print('LinearSVC')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %lin_svc.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)


## K-plus-proches voisins
# K-plus-proches voisins (uniform)
clf7 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', KNeighborsClassifier(10, weights='uniform')),
	])

knn_uniform = clf7.fit(train_text, train_label)
prediction = knn_uniform.predict(test_text)
print('KPPV uniform')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %knn_uniform.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# K-plus-proches voisins (distance)
clf8 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', KNeighborsClassifier(10, weights='distance')),
	])

knn_distance = clf8.fit(train_text, train_label)
prediction = knn_distance.predict(test_text)
print('KPPV distance')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %knn_distance.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)


## Boosting avec arbre de décision
# Boosting avec arbre de décision (entropie de Shannon)
clf9 = Pipeline([('vect', CountVectorizer()),
				('densetransform', DenseTransformer()),
				('clf', AdaBoostClassifier(DecisionTreeClassifier(criterion='entropy'))),
	])

ada_boost_entropy = clf9.fit(train_text, train_label)
prediction = ada_boost_entropy.predict(test_text)
print('Boosting avec arbre de décision (entropie de Shannon)')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %ada_boost_entropy.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Boosting avec arbre de décision(index de gini)
clf10 = Pipeline([('vect', CountVectorizer()),
				('densetransform', DenseTransformer()),
				('clf', AdaBoostClassifier(DecisionTreeClassifier(criterion='gini'))),
	])

ada_boost_gini = clf10.fit(train_text, train_label)
prediction = ada_boost_gini.predict(test_text)
print('Boosting avec arbre de décision (index de gini)')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %ada_boost_gini.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)


## Naive_Bayes
# Naive_Bayes Multinomial
clf11 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', MultinomialNB()),
	])

multinomial = clf11.fit(train_text, train_label)
prediction = multinomial.predict(test_text)
print('Naive_Bayes Multinomial')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %multinomial.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Naive_Bayes Bernoulli
clf12 = Pipeline([('vect', CountVectorizer()),
				('tfidf', TfidfTransformer()),
				('clf', BernoulliNB()),
	])

bernoulli = clf12.fit(train_text, train_label)
prediction = bernoulli.predict(test_text)
print('Naive_Bayes Bernoulli')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %bernoulli.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)

# Naive_Bayes Gaussien
clf13 = Pipeline([('vect', CountVectorizer()),
				('densetransform', DenseTransformer()),
				('clf', GaussianNB()),
	])

gaussien = clf13.fit(train_text, train_label)
prediction = gaussien.predict(test_text)
print('Naive_Bayes Gaussien')
# pourcentage d'exemples bien classés
print("Pourcentage d'exemples bien classés : %0.2f" %(100*accuracy_score(test_label, prediction)))
# matrice de confusion
print("Matrice de confusion :")
print(confusion_matrix(test_label, prediction))
# précision moyenne
print("Précision moyenne : %f" %gaussien.score(test_label, prediction))
print ("Aire sous la courbe ROC : %f" %roc_auc_score(transformationBinaire(test_label),
													transformationBinaire(prediction)))
print('-'*60)
#####################################################################################
