1. installer python 3 sur le site officiel (https://www.python.org/downloads/) de préférence 3.9

2. aller dans le répertoire "\Program Files\Python3X) avec X la version mineure la plus récente

3. taper les commandes suivantes :	python -mpip install -U pip
									python -mpip install -U scikit-learn
La première ligne met à jour le programme pip, qui a pour but de rechercher et d’installer les bibliothèques et programmes
Python.

La deuxième ligne télécharge et installe la bibliothèque scikit-learn.

4. aller sur un éditeur python (j'utilise idle le plus souvent)

5. ouvrir le premier fichier sms_Thierry_Schwartzmann.py

6. exécuter le programme python (raccourci avec la touche F5), puis regarder les résultats sur la console

7. ouvrir le deuxième fichier mail_Thierry_Schwartzmann.py

8. exécuter le programme python (raccourci avec la touche F5), puis regarder les résultats dans le fichier
	resultats_Test_Schwartzmann.txt