#Thierry Schwartzmann (21208197)
#!/usr/lib/python3.4
from os import listdir
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC


train_text=[] # contenu du message pour la base d'entrainement
train_label=[] # label (ham ou spam) pour la base d'entrainement
test_text=[] # contenu du message pour la base de tests
test_label=[] # label (ham ou spam) de la base de tests
mails=[] # liste des noms de tous les emails


## traitement des conversions
# conversion d'une liste en chaine
def conversionEnChaine(liste):
    chaine=""
    for element in liste:
        chaine+=element
    return chaine

# conversion du label en binaire
def conversionBinaire(label):
    if label=="ham":
        return 0
    else:
        return 1

## traitement de la base d'entrainement
# liste des mails dans le répertoire Base_Email/train/easy_ham
easy_ham=listdir("Base_Email/train/easy_ham")

# on parcourt la liste des mails de la base d'entrainement qui sont des easy_ham
for i in range(len(easy_ham)):

    # ouverture du fichier avec résolution des problèmes d'encodage
    fichier=open("Base_Email/train/easy_ham/"+easy_ham[i],"r", encoding='ISO-8859-1')

    # lecture du contenu du fichier
    mail=fichier.read()

    # suppression de l'entete de l'email
    entete=mail.split("\n\n")[1:]
    chaine=conversionEnChaine(entete)
    train_text.append(chaine)

    # ajout du label (ou type) de mail (ham)
    train_label.append("ham")


# liste des mails dans le répertoire Base_Email/train/hard_ham
hard_ham=listdir("Base_Email/train/hard_ham")

# on parcourt la liste des mails de la base d'entrainement qui sont des hard_ham
for i in range(len(hard_ham)):

    # ouverture du fichier avec résolution des problèmes d'encodage
    fichier=open("Base_Email/train/hard_ham/"+hard_ham[i],"r", encoding='ISO-8859-1')

    # lecture du contenu du fichier
    mail=fichier.read()

    # suppression de l'entete de l'email
    entete=mail.split("\n\n")[1:]
    chaine=conversionEnChaine(entete)
    train_text.append(chaine)

    # ajout du label (ou type) de mail (ham)
    train_label.append("ham")


# liste des mails dans le répertoire Base_Email/train/spam
spam=listdir("Base_Email/train/spam")

# on parcourt la liste des emails de la base d'entrainement qui sont des spam
for i in range(len(spam)):

    # ouverture du fichier avec résolution des problèmes d'encodage
    fichier=open("Base_Email/train/spam/"+spam[i],"r", encoding='ISO-8859-1')

    # lecture du contenu du fichier
    mail=fichier.read()

    # suppression de l'entete de l'email
    entete=mail.split("\n\n")[1:]
    chaine=conversionEnChaine(entete)
    train_text.append(chaine)

    # ajout du label (ou type) de mail (spam)
    train_label.append("spam")


## traitement de la base de tests
# liste des mails dans le répertoire Base_Email/test
repertoire=listdir("Base_Email/test")

# on parcourt la liste de la base de test 
for i in range(len(repertoire)):

    # ouverture du fichier avec résolution des problèmes d'encodage
    fichier=open("Base_Email/test/"+repertoire[i],"r", encoding='ISO-8859-1')

    # lecture du contenu du fichier
    mail=fichier.read()

    # ouverture du fichier avec résolution des problèmes d'encodage
    entete=mail.split("\n\n")[1:]
    chaine=conversionEnChaine(entete)
    test_text.append(chaine)

    # ajout du label (ou type) de mail (spam)
    mails.append(repertoire[i])


# définition du classifieur
clf = Pipeline([('vect', CountVectorizer()),
                ('tfidf', TfidfTransformer()),
                ('clf', LinearSVC()),
    ])

lin_svc = clf.fit(train_text, train_label)


# creation du fichier de résultats pour la base de tests
fichierResultat=open("resultats_Test_Schwartzmann.txt","w")

# parcours du contenu des mails dans la base de tests
for i  in range(len(test_text)):

    # prediction pour chaque mail
    prediction = lin_svc.predict([test_text[i]])

    # contenu de chaque ligne
    contenu=mails[i]+" "+ str(conversionBinaire(prediction))+'\n'

    # contenu de chaque ligne
    fichierResultat.write(contenu)

# fermeture du fichier de résultats
fichierResultat.close()

